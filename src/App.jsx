import { Container } from "@chakra-ui/react";
import React, { useCallback, useState } from "react";
import { NodeManager } from "./components/NodeManager";
import { Playground } from "./components/Playground";

const App = () => {
  const [nodes, setNodes] = useState([]);

  const addNode = useCallback((node) => {
    console.log("node", node);
    setNodes((prev) => [...prev, node]);
  }, []);

  return (
    <Container>
      <NodeManager addNode={addNode} />
      <Playground nodes={nodes} />
    </Container>
  );
};

export default App;
