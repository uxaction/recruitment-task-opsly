import React, { useCallback, useState } from "react";
import {
  Box,
  Button,
  ButtonGroup,
  Grid,
  Tabs,
  TabList,
  TabPanels,
  Tab,
  TabPanel,
  FormControl,
  FormLabel,
  Input,
  HStack,
  Tag,
} from "@chakra-ui/react";

import { ENTER_KEY_CODE } from "../constants";

export const NodeManager = ({ addNode }) => {
  const [inPorts, setInPorts] = useState([]);
  const [outPorts, setOutPorts] = useState([]);
  const [tempIn, setTempIn] = useState("");
  const [tempOut, setTempOut] = useState("");
  const [title, setTitle] = useState("");

  const handleTitleChange = useCallback(({ target: { value } }) => {
    setTitle(value);
  }, []);

  const addIn = useCallback(({ target: { value } }) => {
    setTempIn(value);
  }, []);

  const addOut = useCallback(({ target: { value } }) => {
    setTempOut(value);
  }, []);

  const handleInKeyUp = useCallback(
    ({ keyCode }) => {
      if (keyCode === ENTER_KEY_CODE && tempIn.length) {
        setInPorts((prev) => [...prev, tempIn]);
        setTempIn("");
      }
    },
    [tempIn]
  );

  const handleOutKeyUp = useCallback(
    ({ keyCode }) => {
      if (keyCode === ENTER_KEY_CODE && tempOut.length) {
        setOutPorts((prev) => [...prev, tempOut]);
        setTempOut("");
      }
    },
    [tempOut]
  );

  const handleAddNodeClick = useCallback(() => {
    addNode({ title, inPorts, outPorts });
    setTitle("");
    setOutPorts([]);
    setInPorts([]);
  }, [title, inPorts, outPorts, addNode]);

  return (
    <Tabs>
      <TabList>
        <Tab>Node</Tab>
      </TabList>

      <TabPanels>
        <TabPanel>
          <Box mb={4}>
            <Grid templateColumns="repeat(3, 1fr)" gap={6}>
              <FormControl>
                <FormLabel>Title</FormLabel>
                <Input type="text" value={title} onChange={handleTitleChange} />
              </FormControl>
              <FormControl>
                <Box mb={2}>
                  <FormLabel>IN</FormLabel>
                  <Input
                    type="text"
                    value={tempIn}
                    onChange={addIn}
                    onKeyUp={handleInKeyUp}
                  />
                </Box>
                <HStack spacing={4}>
                  {inPorts.map((port) => (
                    <Tag
                      size="sm"
                      key={port}
                      variant="solid"
                      colorScheme="teal"
                    >
                      {port}
                    </Tag>
                  ))}
                </HStack>
              </FormControl>
              <FormControl>
                <Box mb={2}>
                  <FormLabel>OUT</FormLabel>
                  <Input
                    type="text"
                    value={tempOut}
                    onChange={addOut}
                    onKeyUp={handleOutKeyUp}
                  />
                </Box>
                <HStack spacing={4}>
                  {outPorts.map((port) => (
                    <Tag
                      size="sm"
                      key={port}
                      variant="solid"
                      colorScheme="teal"
                    >
                      {port}
                    </Tag>
                  ))}
                </HStack>
              </FormControl>
            </Grid>
          </Box>
          <ButtonGroup>
            <Button onClick={handleAddNodeClick}>Add</Button>
          </ButtonGroup>
        </TabPanel>
      </TabPanels>
    </Tabs>
  );
};
