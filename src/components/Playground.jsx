import React, { Component, createRef } from "react";
import ReactDOM from "react-dom";
import * as joint from "jointjs";

export class Playground extends Component {
  constructor(props) {
    super(props);

    this.graph = new joint.dia.Graph();
    this.playgroundRef = createRef();
  }

  componentDidMount() {
    this.paper = new joint.dia.Paper({
      el: ReactDOM.findDOMNode(this.playgroundRef.current),
      width: 800,
      height: 600,
      model: this.graph,
      gridSize: 1,
      defaultLink: new joint.dia.Link({
        attrs: { ".marker-target": { d: "M 10 0 L 0 5 L 10 10 z" } },
      }),
    });
  }

  componentDidUpdate(prevProps) {
    this.props.nodes
      .filter((node) => !prevProps.nodes.includes(node))
      .forEach((node) => this.addNode(node));
  }

  addNode({ title, inPorts, outPorts }) {
    this.graph.addCells([
      new joint.shapes.devs.Model({
        position: { x: 360, y: 360 },
        inPorts: inPorts,
        outPorts: outPorts,
        ports: {
          groups: {
            in: {
              attrs: {
                ".port-body": {
                  fill: "#16A085",
                  magnet: "passive",
                },
              },
            },
            out: {
              attrs: {
                ".port-body": {
                  fill: "#E74C3C",
                },
              },
            },
          },
        },
        attrs: {
          text: {
            text: title,
          },
        },
      }),
    ]);
  }

  render() {
    return <div ref={this.playgroundRef} />;
  }
}
